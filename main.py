
import customtkinter

from app.app import App

customtkinter.set_appearance_mode("dark")  # Modes: system (default), light, dark
customtkinter.set_default_color_theme("blue")  # Themes: blue (default), dark-blue, green

# Everything starts here
if __name__ == '__main__':
    App().mainloop()

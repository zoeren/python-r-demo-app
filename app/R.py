import subprocess
from pathlib import Path


def get_r_install_path() -> Path:
    return Path('C:/Program Files/R/R-4.3.1/')


def renv_restore(working_directory: Path):
    subprocess.run(
        [get_r_install_path() / 'bin' / 'Rscript.exe', '-e', 'renv::restore()'],
        cwd=working_directory,
        shell=True,
        text=True,
    )


def run_r_script_async(working_directory: Path, script_file_path: Path):
    # renv restore before executing the script. renv is activated when executing a script but packages
    # are not installed automatically. In RStudio this is done by RStudio itself.
    # Another possibility would be to handle this in .Rprofile (this is basically an R script)
    renv_restore(working_directory)

    return subprocess.Popen(
        [get_r_install_path() / 'bin' / 'Rscript.exe', script_file_path],
        cwd=working_directory,
        shell=True,
        text=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

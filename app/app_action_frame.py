import tkinter
from pathlib import Path
from typing import Callable, Any

import customtkinter


class AppActionFrame(customtkinter.CTkFrame):
    def __init__(self, parent, run_script: Callable[[str | Path], Any], **kwargs):
        super().__init__(parent, **kwargs)

        self.run_script_button = customtkinter.CTkButton(
            self,
            text="Run demo_script.R",
            command=lambda: run_script('demo_script.R')
        )
        self.run_script_button.grid(padx=10, pady=10)

    def disable(self):
        self.run_script_button.configure(state=tkinter.DISABLED)

    def enable(self):
        self.run_script_button.configure(state=tkinter.NORMAL)

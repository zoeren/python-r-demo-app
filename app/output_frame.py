import tkinter

import customtkinter


class OutputFrame(customtkinter.CTkScrollableFrame):
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)

        self.log_entries = []

    def add_log_entry(self, message, is_script_output=False):
        label = customtkinter.CTkLabel(
            self,
            text=message,
            text_color='grey' if is_script_output else None,
            font=('Courier', 12) if is_script_output else ('Arial', 12),
            justify=tkinter.LEFT
        )
        label.grid(row=len(self.log_entries), column=0, sticky=tkinter.W, padx=5)

        self.log_entries.append(label)

import threading
import tkinter
from pathlib import Path

import customtkinter

from .R import run_r_script_async, get_r_install_path
from .app_action_frame import AppActionFrame
from .output_frame import OutputFrame


class App(customtkinter.CTk):
    working_directory = Path.cwd() / 'R'
    script_file_name = 'demo_script.R'

    def __init__(self):
        super().__init__()

        self.title("R script application")
        self.geometry(f"{600}x{400}")

        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=0)
        self.grid_rowconfigure(1, weight=0)
        self.grid_rowconfigure(2, weight=1)

        self.actions_frame = AppActionFrame(
            self,
            run_script=lambda script: threading.Thread(target=self.run_script_thread, args=[script]).start()
        )
        self.actions_frame.grid(row=0, column=0, sticky=tkinter.EW, padx=6, pady=6)

        self.progressbar = customtkinter.CTkProgressBar(
            self,
            orientation=tkinter.HORIZONTAL,
            mode="indeterminate",
            indeterminate_speed=0.6
        )
        self.progressbar.start()

        self.script_output_frame = OutputFrame(self)
        self.script_output_frame.grid(row=2, column=0, sticky=tkinter.NSEW, padx=6, pady=6)

        self.initialize()

    def initialize(self):
        self.script_output_frame.add_log_entry("R install directory - %s" % get_r_install_path())
        self.script_output_frame.add_log_entry("Working directory - %s" % self.working_directory)
        self.script_output_frame.add_log_entry("Welcome. You can click the button above to see what happens.")

    def run_script_thread(self, script_file):
        self.actions_frame.disable()
        self.show_progressbar()

        process = run_r_script_async(
            working_directory=self.working_directory,
            script_file_path=self.working_directory / script_file
        )
        stdout, stderr = process.communicate()

        stdout = stdout.strip()
        if len(stdout) > 0:
            self.script_output_frame.add_log_entry(stdout, is_script_output=True)

        if process.returncode == 0:
            self.script_output_frame.add_log_entry("Script finished successfully")
        else:
            stderr = stderr.strip()
            if len(stderr) > 0:
                self.script_output_frame.add_log_entry(stderr, is_script_output=True)

            self.script_output_frame.add_log_entry("Script execution finished with error %d" % process.returncode)

        self.actions_frame.enable()
        self.hide_progressbar()

    def show_progressbar(self):
        self.progressbar.grid(row=1, column=0, sticky=tkinter.EW, padx=6, pady=3)

    def hide_progressbar(self):
        self.progressbar.grid_forget()

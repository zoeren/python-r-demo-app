@ECHO OFF

IF EXIST dist RMDIR /S /Q dist
IF EXIST build RMDIR /S /Q build

call ".venv/Scripts/activate.bat"

pyinstaller demo_app.spec

call ".venv/Scripts/deactivate.bat"
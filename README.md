# Python R Demo App

## Prerequisites

- Python 3 (tested with 3.12)
- R 4 (tested with 4.3.1)

## Usage

- setup python environment by running `app_setup.bat`
- develop R scripts in R folder
- build your app by running `app_build.bat` and enjoy the content of `dist/`

